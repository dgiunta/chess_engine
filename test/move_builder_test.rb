require 'test_helper'

module Chess
  describe MoveBuilder do
    describe "figuring out the disambiguator" do
      it "returns nil if no disambiguation is needed" do
        knight_1 = Knight.new(color: :white, position: Position[:a, 2])
        knight_2 = Knight.new(color: :white, position: Position[:c, 2])
        board = Board.new(pieces: [knight_1, knight_2])

        to_position = Position[:c, 3]

        builder = MoveBuilder.new(board: board, piece: knight_1, to_position: to_position)

        _(builder.disambiguator).must_be_nil
      end

      it "figures out the disambiguating file" do
        knight_1 = Knight.new(color: :white, position: Position[:a, 2])
        knight_2 = Knight.new(color: :white, position: Position[:c, 2])
        board = Board.new(pieces: [knight_1, knight_2])

        to_position = Position[:b, 4]

        builder = MoveBuilder.new(board: board, piece: knight_1, to_position: to_position)

        _(builder.disambiguator).must_equal Position[:a, nil]
      end

      it "figures out the disambiguating rank" do
        knight_1 = Knight.new(color: :white, position: Position[:a, 2])
        knight_2 = Knight.new(color: :white, position: Position[:a, 4])
        board = Board.new(pieces: [knight_1, knight_2])

        to_position = Position[:c, 3]

        builder = MoveBuilder.new(board: board, piece: knight_2, to_position: to_position)

        _(builder.disambiguator).must_equal Position[nil, 4]
      end

      it "figures out the disambiguating file and rank" do
        knight_1 = Knight.new(color: :white, position: Position[:a, 4])
        knight_2 = Knight.new(color: :white, position: Position[:e, 2])
        knight_3 = Knight.new(color: :white, position: Position[:e, 4])
        board = Board.new(pieces: [knight_1, knight_2, knight_3])

        to_position = Position[:c, 3]

        builder = MoveBuilder.new(board: board, piece: knight_3, to_position: to_position)

        _(builder.disambiguator).must_equal Position[:e, 4]
      end
    end
  end
end
