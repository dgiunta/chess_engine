require 'test_helper'

module Chess
  describe King do
    describe "move pattern" do
      it "returns offsets for one space different in all directions" do
        king = King.new(color: :white, position: Position[:e, 4])
        king.has_moved = true

        board = Board.new(pieces: [king])

        result = king.move_pattern_in(board)
        _(result).must_include [[0, -1]] # up
        _(result).must_include [[1, -1]] # right and up
        _(result).must_include [[1, 1]] # right and down
        _(result).must_include [[0, 1]] # down
        _(result).must_include [[-1, 1]] # left and down
        _(result).must_include [[-1, -1]] # left and up

        _(result).must_include [[-1, 0]] # left without castle
        _(result).must_include [[1, 0]] # right without castle
      end

      describe "castle positions" do
        let(:king) { King.new(color: :white, position: Position[:e, 1]) }
        let(:queenside_rook) { Rook.new(color: :white, position: Position[:a, 1]) }
        let(:kingside_rook) { Rook.new(color: :white, position: Position[:h, 1]) }

        let(:board) { Board.new(pieces: [king, queenside_rook, kingside_rook]) }
        let(:result) { king.move_pattern_in(board) }

        it "includes them when all cases are satisfied for a castle" do
          _(result).must_include [[-1, 0], [-2, 0]] # left with castle
          _(result).must_include [[1, 0], [2, 0]] # right with castle
        end

        it "does not include them when the king has moved" do
          king.has_moved = true

          # queenside - castle not allowed
          _(king.castleable?(board, :queen)).must_equal false
          _(result).wont_include [[-1, 0], [-2, 0]]
          _(result).must_include [[-1, 0]]

          # kingside - castle not allowed
          _(king.castleable?(board, :king)).must_equal false
          _(result).wont_include [[1, 0], [2, 0]]
          _(result).must_include [[1, 0]]
        end

        it "does not include them when the king has been in check" do
          king.has_been_checked = true

          # queenside - castle not allowed
          _(king.castleable?(board, :queen)).must_equal false
          _(result).wont_include [[-1, 0], [-2, 0]]
          _(result).must_include [[-1, 0]]

          # kingside - castle not allowed
          _(king.castleable?(board, :king)).must_equal false
          _(result).wont_include [[1, 0], [2, 0]]
          _(result).must_include [[1, 0]]
        end

        it "does not include them when the rook has moved" do
          queenside_rook.has_moved = true
          kingside_rook.has_moved = false

          # queenside - castle not allowed
          _(king.castleable?(board, :queen)).must_equal false
          _(result).wont_include [[-1, 0], [-2, 0]]
          _(result).must_include [[-1, 0]]

          # kingside - castle allowed
          _(king.castleable?(board, :king)).must_equal true
          _(result).must_include [[1, 0], [2, 0]]
          _(result).wont_include [[1, 0]]
        end

        it "does not include them when there are pieces in between the rook and king" do
          # put a piece in between the queenside rook and king
          bishop = Bishop.new(color: :white, position: Position[:c, 1])
          board.pieces << bishop

          # queenside - castle not allowed
          _(king.castleable?(board, :queen)).must_equal false
          _(result).wont_include [[-1, 0], [-2, 0]]
          _(result).must_include [[-1, 0]]

          # kingside - castle allowed
          _(king.castleable?(board, :king)).must_equal true
          _(result).wont_include [[1, 0]]
          _(result).must_include [[1, 0], [2, 0]]
        end
      end
    end
  end
end
