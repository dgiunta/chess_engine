require 'test_helper'

module Chess
  describe Pawn do
    describe "#move_positions_in(board)" do
      it "does normal moves correctly" do
        board = Board.new(pieces: [
          white_pawn = Pawn.new(color: :white, position: Position[:b, 2])
        ])

        move_positions = white_pawn.move_positions_in(board).flatten
        _(move_positions.length).must_equal 2
        _(move_positions).must_include(Position[:b, 3])
        _(move_positions).must_include(Position[:b, 4])
      end

      it "does not include the second position if the pawn has moved already" do
        board = Board.new(pieces: [
          white_pawn = Pawn.new(color: :white, position: Position[:b, 2])
        ])

        white_pawn.has_moved = true

        move_positions = white_pawn.move_positions_in(board).flatten
        _(move_positions.length).must_equal 1
        _(move_positions).must_include(Position[:b, 3])
        _(move_positions).wont_include(Position[:b, 4])
      end

      it "does not include the second position if there's a piece in the way" do
        board = Board.new(pieces: [
          white_pawn = Pawn.new(color: :white, position: Position[:b, 2]),
          Bishop.new(color: :black, position: Position[:b, 4])
        ])

        move_positions = white_pawn.move_positions_in(board).flatten
        _(move_positions.length).must_equal 1
        _(move_positions).must_include(Position[:b, 3])
        _(move_positions).wont_include(Position[:b, 4])
      end

      it "does not include the first or second positions if there's a piece in the way" do
        board = Board.new(pieces: [
          white_pawn = Pawn.new(color: :white, position: Position[:b, 2]),
          Bishop.new(color: :black, position: Position[:b, 3])
        ])

        move_positions = white_pawn.move_positions_in(board).flatten
        _(move_positions.length).must_equal 0
        _(move_positions).wont_include(Position[:b, 3])
        _(move_positions).wont_include(Position[:b, 4])
      end
    end

    describe "attackable_positions_in(board)" do
      it "includes both diagonal positions" do
        board = Board.new(pieces: [
          white_pawn = Pawn.new(color: :white, position: Position[:b, 2])
        ])

        attackable_positions = white_pawn.attackable_positions_in(board).flatten
        _(attackable_positions.length).must_equal 2
        _(attackable_positions).must_include Position[:a, 3]
        _(attackable_positions).must_include Position[:c, 3]
      end
    end

    describe "#available_positions_in(board)" do
      it "includes the possible move positions" do
        board = Board.new(pieces: [
          white_pawn = Pawn.new(color: :white, position: Position[:b, 2])
        ])

        available_positions = white_pawn.available_positions_in(board)
        _(available_positions.length).must_equal 2
        _(available_positions).must_include Position[:b, 3]
        _(available_positions).must_include Position[:b, 4]
      end

      it "includes attack positions for opposing pieces sitting on diagonals" do
        board = Board.new(pieces: [
          white_pawn = Pawn.new(color: :white, position: Position[:b, 2]),
          Bishop.new(color: :black, position: Position[:c, 3])
        ])

        available_positions = white_pawn.available_positions_in(board)
        _(available_positions.length).must_equal 3

        # normal moves
        _(available_positions).must_include Position[:b, 3]
        _(available_positions).must_include Position[:b, 4]

        # attack positions
        _(available_positions).must_include Position[:c, 3]
        _(available_positions).wont_include Position[:a, 3]
      end

      it "includes attack positions for en passant pieces" do
        board = Board.new(pieces: [
          King.new(color: :white, position: Position[:e, 1]),
          King.new(color: :black, position: Position[:e, 8]),
          white_pawn = Pawn.new(color: :white, position: Position[:b, 5]),
          black_pawn = Pawn.new(color: :black, position: Position[:a, 7])
        ], moves: [Move.new(color: :black, piece_class: Pawn, position: Position[:a, 5])])

        white_pawn.has_moved = true

        available_positions = white_pawn.available_positions_in(board)
        _(available_positions.length).must_equal 2

        # normal moves
        _(available_positions).must_include Position[:b, 6]

        # attack positions
        _(available_positions).must_include Position[:a, 6]
        _(available_positions).wont_include Position[:c, 6]
      end
    end
  end
end
