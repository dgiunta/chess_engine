require 'test_helper'

module Chess
  describe Rook do
    describe "initializing with a side" do
      it "is :queen when the file position is :a" do
        rook = Rook.new(color: :white, position: Position[:a, 1])
        _(rook.side).must_equal :queen
      end

      it "is :king when the file position is :h" do
        rook = Rook.new(color: :white, position: Position[:h, 1])
        _(rook.side).must_equal :king
      end

      it "is nil when the file position is anything else" do
        rook = Rook.new(color: :white, position: Position[:e, 1])
        _(rook.side).must_be_nil
      end
    end
  end
end
