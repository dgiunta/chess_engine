require 'test_helper'

module Chess
  describe Piece do
    describe "#available_positions_in(board)" do
      let(:board) { Board.new }

      it "includes all positions in diagonal directions" do
        piece = Bishop.new(color: :white, position: Position[:d, 4])
        board.pieces = [piece]

        available_positions = piece.available_positions_in(board)
        _(available_positions.length).must_equal(13)

        # up and to the right
        _(available_positions).must_include Position[:e, 5]
        _(available_positions).must_include Position[:f, 6]
        _(available_positions).must_include Position[:g, 7]
        _(available_positions).must_include Position[:h, 8]

        # up and to the left
        _(available_positions).must_include Position[:c, 5]
        _(available_positions).must_include Position[:b, 6]
        _(available_positions).must_include Position[:a, 7]

        # down and to the right
        _(available_positions).must_include Position[:e, 3]
        _(available_positions).must_include Position[:f, 2]
        _(available_positions).must_include Position[:g, 1]

        # down and to the left
        _(available_positions).must_include Position[:c, 3]
        _(available_positions).must_include Position[:b, 2]
        _(available_positions).must_include Position[:a, 1]
      end

      it "excludes positions beyond any existing piece" do
        piece = Bishop.new(color: :white, position: Position[:d, 4])
        [:white, :black].each do |other_color|
          other_piece = Pawn.new(color: other_color, position: Position[:f, 6])
          board.pieces = [piece, other_piece]

          available_positions = piece.available_positions_in(board)
          _(available_positions.length).must_equal(11)

          # up and to the right
          _(available_positions).must_include Position[:e, 5]
          _(available_positions).must_include Position[:f, 6]
          _(available_positions).wont_include Position[:g, 7]
          _(available_positions).wont_include Position[:h, 8]

          # up and to the left
          _(available_positions).must_include Position[:c, 5]
          _(available_positions).must_include Position[:b, 6]
          _(available_positions).must_include Position[:a, 7]

          # down and to the right
          _(available_positions).must_include Position[:e, 3]
          _(available_positions).must_include Position[:f, 2]
          _(available_positions).must_include Position[:g, 1]

          # down and to the left
          _(available_positions).must_include Position[:c, 3]
          _(available_positions).must_include Position[:b, 2]
          _(available_positions).must_include Position[:a, 1]
        end
      end
    end
  end
end
