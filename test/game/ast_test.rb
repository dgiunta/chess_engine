require 'test_helper'

module Chess
  describe "Chess::Game::AST" do
    include ParserHelpers

    let(:ast) { Game::AST.new }

    it "replaces {integer: string} with real integers" do
      _(ast.apply({integer: "1"})).must_equal 1
    end

    it "replaces {symbol: string} with real symbols" do
      _(ast.apply({symbol: "a"})).must_equal :a
    end

    it "replaces {bool: string} with real booleans" do
      _(ast.apply({bool: "a"})).must_equal(true)
      _(ast.apply({bool: nil})).must_equal(false)
      _(ast.apply({bool: ""})).must_equal(false)
    end

    it "replaces { piece_ident: [QKBNRP] } with the appropriate Piece subclass" do
      Piece::KLASSES.each do |identifier, klass|
        _(ast.apply({piece_ident: identifier})).must_equal(klass)
      end
    end

    it "replaces {file: ..., rank: ...} with Position objects" do
      _(ast.apply({file: :a, rank: 1})).must_equal Position.new(file: :a, rank: 1)
    end

    describe "translating moves" do
      def move_tree(**args)
        {
          piece: nil,
          position_1: nil,
          is_capture: false,
          position_2: nil,
          promoted_to: nil,
          is_check: false,
          is_checkmate: false
        }.merge(args)
      end

      it "replaces move_hashes with Move objects" do
        tree = move_tree(
          piece: (piece_class = Knight),
          position_1: (position = Position.new(file: :a, rank: 3))
        )
        expected = Move.new(piece_class: piece_class, position: position)

        _(ast.apply(tree)).must_equal expected
      end

      describe "adding color to moves" do
        it "sets the color onto the move object" do
          tree = {white: move_tree, black: move_tree}

          result = ast.apply(tree)

          _(result[0].color).must_equal(:white)
          _(result[1].color).must_equal(:black)
        end

        it "for castles it adds the rank based on the color of the move" do
          tree = {white: {castle: "0-0"}, black: {castle: "0-0-0"}}

          result = ast.apply(tree)

          _(result[0].position.rank).must_equal(1)
          _(result[1].position.rank).must_equal(8)
        end
      end

      describe "translating a castle" do
        it "replaces a king's side {castle: '0-0'} with Castle objects and a position on the :g file" do
          tree = {castle: "0-0"}
          expected = Move.new(piece_class: King, position: Position.new(file: :g, rank: nil), is_castle: true)

          _(ast.apply(tree)).must_equal expected
        end

        it "replaces a queen's side {castle: '0-0-0'} with Castle objects and a position on the :c file" do
          tree = {castle: "0-0-0"}
          expected = Move.new(piece_class: King, position: Position.new(file: :c, rank: nil), is_castle: true)

          _(ast.apply(tree)).must_equal expected
        end
      end
    end

    describe "rounds" do
      it "converts {round: i, moves: []} to round objects" do
        moves = [
          Move.new(color: :white, piece_class: Pawn, position: Position[:c, 3]),
          Move.new(color: :black, piece_class: Pawn, position: Position[:h, 6])
        ]
        tree = {round: 1, moves: moves}

        result = ast.apply(tree)

        _(result.number).must_equal 1
        _(result.moves).must_equal moves
      end
    end
  end
end
