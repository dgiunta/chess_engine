require 'test_helper'

module Chess
  describe "Chess::Game::Parser" do
    include ParserHelpers

    let(:parser) { Game::Parser.new }

    describe "round numbers" do
      it "pulls out the number followed by a period" do
        _(parser.round_num.parse("1.")).must_equal(round_num(1))
        _(parser.round_num.parse("1.")).must_equal(round_num(1))
      end

      it "ignores trailing spaces" do
        _(parser.round_num.parse("1. ")).must_equal(round_num(1))
      end

      it "works with two or more digits" do
        _(parser.round_num.parse("10.")).must_equal(round_num(10))
        _(parser.round_num.parse("100.")).must_equal(round_num(100))
      end
    end

    describe "files" do
      it "pulls out letters a-h" do
        ('a'..'h').each do |char|
          _(parser.file.parse(char)).must_equal(file: {symbol: char})
        end
      end

      it "does not parse any other letters" do
        _ { parser.file.parse("i") }.must_raise(Parslet::ParseFailed)
      end
    end

    describe "ranks" do
      it "pulls out numbers 1-8" do
        (1..8).each do |num|
          _(parser.rank.parse(num.to_s)).must_equal(rank: {integer: num.to_s})
        end
      end

      it "does not parse any other numbers" do
        _ { parser.rank.parse("0") }.must_raise(Parslet::ParseFailed)
        _ { parser.rank.parse("9") }.must_raise(Parslet::ParseFailed)
      end
    end

    describe "positions" do
      it "pulls out file and rank from two-character move strings" do
        _(parser.position.parse("a3")).must_equal(position(:a, 3))
      end

      it "pulls out just file one-character move string" do
        _(parser.position.parse("a")).must_equal(position(:a, nil))
      end

      it "pulls out just rank one-character move string" do
        _(parser.position.parse("3")).must_equal(position(nil, 3))
      end

      it "returns nils for an emnpty string" do
        _(parser.position.parse("")).must_equal(position(nil, nil))
      end
    end

    describe "piece identifiers" do
      it "recognizes specific uppercase piece characters" do
        Piece::KLASSES.keys.each do |char|
          _(parser.piece?.parse(char)).must_equal(piece(char))
        end
      end

      it "returns nil for blank strings" do
        _(parser.piece?.parse("")).must_equal(piece(nil))
      end

      it "does not recognize lowercase letters" do
        _ { parser.piece?.parse("p") }.must_raise(Parslet::ParseFailed)
      end

      it "does not recognize non-piece letters" do
        _ { parser.piece?.parse("a") }.must_raise(Parslet::ParseFailed)
      end
    end

    describe "captures" do
      it "recognizes x" do
        _(parser.is_capture?.parse("x")).must_equal(is_capture("x"))
      end

      it "recognizes :" do
        _(parser.is_capture?.parse(":")).must_equal(is_capture(":"))
      end

      it "returns nil for blank strings" do
        _(parser.is_capture?.parse("")).must_equal(is_capture(nil))
      end

      it "returns doesn't recognize anything but x or :" do
        _ { parser.is_capture?.parse("a") }.must_raise(Parslet::ParseFailed)
      end
    end

    describe "moves" do
      it "recognizes a move without a piece" do
        expected = move(position_1: position(:e, 4))
        _(parser.move.parse("e4")).must_equal(expected)
      end

      describe "a collection of moves" do
        it "recognizes when both white and black moves have been given" do
          expected = {
            white: move(
              position_1: position(:e, 4)
            ),

            black: move(
              position_1: position(:e, 6)
            )
          }

          _(parser.moves.parse("e4 e6")).must_equal(expected)
        end

        it "recognizes when only white has been given" do
          expected = {
            white: move(
              position_1: position(:e, 4)
            ),

            black: move()
          }

          _(parser.moves.parse("e4")).must_equal(expected)
        end
      end

      describe "without a capture" do
        it "recognizes a move without a capture and puts the to position in position_1" do
          expected = move(
            piece: piece("N"),
            position_1: position(:e, 4)
          )
          _(parser.move.parse("Ne4")).must_equal(expected)
        end

        it "recognizes a move with a file-based disambiguator in position_1 and puts the to position in position_2" do
          expected = move(
            piece: piece("N"),
            position_1: position(:f, nil),
            position_2: position(:e, 4)
          )
          _(parser.move.parse("Nfe4")).must_equal(expected)
        end

        it "recognizes a move with a rank-based disambiguator in position_1 and puts the to position in position_2" do
          expected = move(
            piece: piece("N"),
            position_1: position(nil, 3),
            position_2: position(:e, 4)
          )
          _(parser.move.parse("N3e4")).must_equal(expected)
        end

        it "recognizes a move with a rank and file-based disambiguator in position_1 and puts the to position in position_2" do
          expected = move(
            piece: piece("N"),
            position_1: position(:f, 3),
            position_2: position(:e, 4)
          )
          _(parser.move.parse("Nf3e4")).must_equal(expected)
        end
      end

      describe "with a capture" do
        it "recognizes a move with a capture and puts the to position in position_2" do
          expected = move(
            piece: piece("N"),
            is_capture: is_capture("x"),
            position_2: position(:e, 4)
          )
          _(parser.move.parse("Nxe4")).must_equal(expected)
        end

        it "recognizes a move with a file-based disambiguator in position_1 and puts the to position in position_2" do
          expected = move(
            piece: piece("N"),
            position_1: position(:f, nil),
            is_capture: is_capture("x"),
            position_2: position(:e, 4)
          )
          _(parser.move.parse("Nfxe4")).must_equal(expected)
        end

        it "recognizes a move with a rank-based disambiguator in position_1 and puts the to position in position_2" do
          expected = move(
            piece: piece("N"),
            position_1: position(nil, 3),
            is_capture: is_capture("x"),
            position_2: position(:e, 4)
          )
          _(parser.move.parse("N3xe4")).must_equal(expected)
        end

        it "recognizes a move with a rank and file-based disambiguator in position_1 and puts the to position in position_2" do
          expected = move(
            piece: piece("N"),
            position_1: position(:f, 3),
            is_capture: is_capture("x"),
            position_2: position(:e, 4)
          )
          _(parser.move.parse("Nf3xe4")).must_equal(expected)
        end
      end

      describe "pawn promotion" do
        it "recognizes a piece identifier at the end of a move" do
          expected = move(
            position_1: position(:f, 8),
            promoted_to: piece_ident("Q")
          )

          _(parser.move.parse("f8Q")).must_equal(expected)
        end

        it "recognizes an optional = before the piece identifier" do
          expected = move(
            position_1: position(:f, 8),
            promoted_to: piece_ident("Q")
          )

          _(parser.move.parse("f8=Q")).must_equal(expected)
        end

        it "returns nil when no piece identifier is at the end of a move" do
          expected = move(
            position_1: position(:f, 8)
          )

          _(parser.move.parse("f8")).must_equal(expected)
        end
      end

      describe "check" do
        it "recognizes a + at the end of a move" do
          expected = move(
            position_1: position(:f, 8),
            is_check: true
          )

          _(parser.move.parse("f8+")).must_equal(expected)
          _(parser.move.parse("f8 +")).must_equal(expected)
        end
      end

      describe "checkmate" do
        it "recognizes a # at the end of a move" do
          expected = move(
            position_1: position(:f, 8),
            is_checkmate: true
          )

          _(parser.move.parse("f8#")).must_equal(expected)
        end
      end

      describe "castling" do
        it "parses a castle" do
          _(parser.castle.parse("0-0")).must_equal(castle: "0-0")
          _(parser.castle.parse("0-0-0")).must_equal(castle: "0-0-0")
        end
      end
    end

    describe "a round" do
      it "parses a full round" do
        expected = round(
          number: 1,
          white: move(position_1: position(:e, 4)),
          black: move(position_1: position(:e, 6))
        )
        _(parser.round.parse("1. e4 e6")).must_equal(expected)
      end

      it "parses a white-only round" do
        expected = round(
          number: 1,
          white: move(position_1: position(:e, 4))
        )
        _(parser.round.parse("1. e4")).must_equal(expected)
      end
    end

    describe "a game" do
      it "parses multiple rounds" do
        expected = {
          rounds: [
            round(
              number: 1,
              white: move(position_1: position(:e, 4)),
              black: move(position_1: position(:e, 6))
            ),
            round(
              number: 2,
              white: move(piece: piece("N"), position_1: position(:c, 3)),
              black: move(piece: piece("N"), position_1: position(:g, 6))
            )
          ]
        }
        _(parser.game.parse("1. e4 e6 2. Nc3 Ng6")).must_equal(expected)
      end
    end
  end
end
