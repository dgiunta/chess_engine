$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "chess"
require 'pry'

require "minitest/autorun"
require "minitest/focus"

module Chess
  module ParserHelpers
    def round_num(i)
      { round: {integer: i.to_s} }
    end

    def piece_ident(char)
      { piece_ident: char }
    end

    def piece(char)
      { piece: piece_ident(char) }
    end

    def position(file, rank)
      {
        file: (file && {symbol: file.to_s}),
        rank: (rank && {integer: rank.to_s})
      }
    end

    def is_capture(char)
      { is_capture: {bool: char} }
    end

    def move(
      piece: piece(nil),
      position_1: position(nil, nil),
      is_capture: is_capture(nil),
      position_2: position(nil, nil),
      promoted_to: nil,
      is_check: false,
      is_checkmate: false
    )
      {}
        .merge(piece)
        .merge(position_1: position_1)
        .merge(is_capture)
        .merge(position_2: position_2)
        .merge(promoted_to: promoted_to)
        .merge(is_check: {bool: is_check ? "+" : nil})
        .merge(is_checkmate: {bool: is_checkmate ? "#" : nil})
    end

    def round(number: 1, white: move, black: move)
      {}
        .merge(round_num(number))
        .merge(moves: { white: white, black: black })
    end
  end
end
