require 'test_helper'
module Chess
  describe AvailablePositionsCache do
    it "does not allow kings to move into check" do
      board = Board.new(pieces: [
        white_king = King.new(color: :white, position: Position[:e, 3]),
        black_bishop = Bishop.new(color: :black, position: Position[:g, 4])
      ])

      _(board.available_moves[white_king]).wont_include Position[:f, 3]
      _(board.available_moves[white_king]).wont_include Position[:e, 2]
    end

    it "does not allow kings to take a protected piece" do
      board = Board.new(pieces: [
        white_king = King.new(color: :white, position: Position[:e, 2]),
        black_bishop = Bishop.new(color: :black, position: Position[:f, 3]),
        black_pawn = Pawn.new(color: :black, position: Position[:g, 4]),
      ])
      _(board.available_moves[white_king]).wont_include Position[:f, 3]
    end
  end
end
