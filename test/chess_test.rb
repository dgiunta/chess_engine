require "test_helper"

describe Chess do
  it "has a version number" do
    _(::Chess::VERSION).wont_be_nil
  end

  describe ".game_from_string" do
    it "parses a whole game string and returns a game object" do
      game = "1. e4 e6 2. Nc3 Nf6"
      _(Chess.game_from_string(game)).must_be_kind_of(Chess::Game)
    end
  end
end
