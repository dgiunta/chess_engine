require "chess/version"

require 'parslet'

require 'chess/game/parser'
require 'chess/game/ast'

require 'chess/piece'

require 'chess/position'
require 'chess/game'
require 'chess/move'
require 'chess/move_builder'
require 'chess/round'
require 'chess/board'
require 'chess/available_positions_cache'

require 'chess/cli'

module Chess
  def self.game_from_string(str)
    ast.apply(parser.parse(str))
  end

  def self.parser
    @parser ||= Game::Parser.new
  end

  def self.ast
    @ast ||= Game::AST.new
  end

  def self.debug
    if block_given?
      @debug = true
      yield
      @debug = false
    else
      @debug
    end
  end

  def self.debug=(bool)
    @debug = bool
  end

  def self.debug!
    @debug = true
  end

  def self.reset_debug!
    @debug = false
  end
end
