module Chess
  class Board
    attr_accessor :moves, :captured_pieces
    attr_writer :pieces

    def initialize(moves: [], pieces: nil, captured_pieces: [])
      @moves = moves
      @pieces = pieces
      @captured_pieces = captured_pieces

      apply_moves!
    end

    def pieces
      @pieces ||= Game::STARTING_PIECES.map {|color, klass, file, rank| klass.new(color: color, position: Position[file, rank]) }
    end

    def to_s(selected_piece=nil)
      [CLI::Board.new(board: self, selected_piece: selected_piece), "Move #{moves.length}: #{moves.last}"].join("\n\n")
    end

    def grouped_pieces
      @grouped_pieces ||= pieces.inject({}) do |out, piece|
        out[piece.color] ||= {}
        out[piece.color][piece.class] ||= []
        out[piece.color][piece.class] << piece
        out
      end
    end

    def available_moves
      AvailablePositionsCache.new(self)
    end

    def apply_moves!
      moves.each(&method(:apply))
    end

    def apply(move)
      move.piece = find_piece_for(move)
      if move.capture?
        captured_piece = find_captured_piece_for(move)
        @pieces -= [captured_piece]
        @captured_pieces << captured_piece
      end

      if move.castle?
        rook = grouped_pieces[move.color][Rook].detect {|r| r.side == move.side }
        direction = rook.side == :queen ? 1 : -1
        rook.move_to(move.position.offset_by(direction, 0))
      end

      move.piece.move_to(move.position)
    end

    def piece_at_position(position)
      pieces.detect {|p| p.position == position }
    end

    def find_piece_for(move)
      grouped_pieces[move.color][move.piece_class]
        .detect do |piece|
          if move.disambiguator
            piece.position.similar_to?(move.disambiguator)
          else
            available_moves.positions_by_piece(piece).include?(move.position)
          end
        end
    end

    def find_captured_piece_for(move)
      captured_piece = piece_at_position(move.position)
      captured_piece ||= piece_at_position(move.position.offset_by(0, move.piece.move_direction)) if move.piece.pawn?

      raise "Can't find piece for move #{move}" if captured_piece.nil?
      captured_piece
    end
  end
end
