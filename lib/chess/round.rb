module Chess
  class Round
    attr_reader :number, :moves

    def initialize(number:, moves: [])
      @number, @moves = number, moves
    end

    def summarize
      "#{number}. #{moves.map(&:summarize).join(" ")}"
    end

    def current_color
      white.nil? || complete? ? :white : :black
    end

    def incomplete?
      !complete?
    end

    def complete?
      white? && black?
    end

    def white?
      !!white
    end

    def black?
      !!black
    end

    def white
      moves[0]
    end

    def white=(move)
      moves[0] = move
      self
    end

    def black
      moves[1]
    end

    def black=(move)
      moves[1] = move
      self
    end
  end
end
