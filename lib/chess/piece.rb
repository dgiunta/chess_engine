module Chess
  class Piece
    require 'chess/pieces/queen'
    require 'chess/pieces/king'
    require 'chess/pieces/bishop'
    require 'chess/pieces/knight'
    require 'chess/pieces/rook'
    require 'chess/pieces/pawn'

    KLASSES = {
      "P" => Pawn,
      "Q" => Queen,
      "K" => King,
      "B" => Bishop,
      "N" => Knight,
      "R" => Rook
    }.each do |_, klass|
      define_method("#{klass.to_s.split("::").last.downcase}?") do
        self.class == klass
      end
    end

    COLORS = %i[white black].each do |color|
      define_method("#{color}?") do
        self.color == color
      end
    end

    def self.summarize
      type_abbrev unless self == Pawn
    end

    def self.class_from(identifier)
      KLASSES[(identifier || "P").to_s.upcase] rescue nil
    end

    def self.type_abbrev
      KLASSES.invert[self]
    end

    def self.name
      self.to_s.split("::").last
    end

    attr_reader :color
    attr_accessor :has_moved, :position, :previous_positions
    alias_method :has_moved?, :has_moved

    def initialize(color: nil, position: nil)
      @color = color
      @position = position
      @previous_positions = []
    end

    def ==(other)
      self.class == other.class &&
        color == other.color &&
        position == other.position
    end

    def id
      [abbrev, position].join(":")
    end

    def type_abbrev
      @type_abbrev ||= self.class.type_abbrev
    end

    def color_abbrev
      @color_abbrev ||= Game::COLORS.invert[color]
    end

    def abbrev
      @abbrev ||= [color_abbrev, type_abbrev].join
    end

    def name
      [color.capitalize, self.class.name].join(" ")
    end

    def to_s
       "#{name} at #{position}"
    end

    def has_not_moved?
      !has_moved?
    end

    def move_to(position)
      self.previous_positions << self.position
      self.position = position
      self.has_moved = true
      self
    end

    MOVE_PATTERN = []
    def move_pattern_in(board)
      self.class::MOVE_PATTERN
    end

    def captureable_by?(other)
      other && color != other.color
    end

    def attackable_positions_in(board)
      move_pattern_in(board).map do |path|
        path.map do |offset_file, offset_rank|
          position.offset_by(offset_file, offset_rank)
        end.compact
      end.map do |path|
        available_positions_from(board, path, pass_through_king: true)
      end
    end

    # Attack positions show all of the places that a piece
    # can attack. Basically, this list should include all positions
    # up to and including any opposing pieces that could be taken,
    # as well as all of the positions that an opposing king would be
    # in check by this piece. Hence, the pass_through_king: true option.
    def attack_positions_in(board)
      attackable_positions_in(board)
    end

    # In most cases, a piece's attack positions are the same as their move positions.
    # Meaning that a piece can take any other piece that is at the end of a path of possible moves.
    #
    # That said, pawns in particular do not have this quality. Pawns can only attack on diagonals
    # but move vertically and only in one direction. Hence the separation of these concepts.
    def move_positions_in(board)
      attack_positions_in(board).map do |path|
        available_positions_from(board, path, pass_through_king: false)
      end
    end

    def available_positions_in(board)
      move_positions_in(board).flatten
    end

    def available_positions_from(board, positions, include_existing_piece: true, pass_through_king: true)
      positions.reduce([false, []]) do |(path_is_done, out), position|
        existing_piece = board.piece_at_position(position)
        position_present = !position.nil?
        existing_piece_present = !existing_piece.nil?
        existing_piece_is_opposing_king = pass_through_king && existing_piece && existing_piece.king? && existing_piece.color != self.color

        next_path_is_done = path_is_done || (position_present && existing_piece_present && !existing_piece_is_opposing_king)

        out << position unless path_is_done || (existing_piece_present && !include_existing_piece)

        [next_path_is_done, out]
      end.last
    end
  end
end
