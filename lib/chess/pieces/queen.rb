module Chess
  class Queen < Piece
    MOVE_PATTERN = [
      (1..8).map {|i| [0, -i] }, # up
      (1..8).map {|i| [i, -i] }, # right and up
      (1..8).map {|i| [i, 0] }, # right
      (1..8).map {|i| [i, i] }, # right and down
      (1..8).map {|i| [0, i] }, # down
      (1..8).map {|i| [-i, i] }, # left and down
      (1..8).map {|i| [-i, 0] }, # left
      (1..8).map {|i| [-i, -i] } # left and up
    ]
  end
end
