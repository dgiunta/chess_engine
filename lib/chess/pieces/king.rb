module Chess
  class King < Piece
    attr_accessor :has_been_checked
    alias_method :has_been_checked?, :has_been_checked

    def has_not_been_checked?
      !has_been_checked?
    end

    def move_pattern_in(board)
      queenside_move = castleable?(board, :queen) ? [[-1, 0], [-2, 0]] : [[-1, 0]]
      kingside_move = castleable?(board, :king) ? [[1, 0], [2, 0]] : [[1, 0]]

      [
        [0, -1], [1, -1], [1, 1], [0, 1], [-1, 1], [-1, -1],
      ].map {|move| [move] } + [queenside_move, kingside_move]
    end

    def castleable?(board, side)
      return false unless has_not_been_checked? && has_not_moved?

      rook = board.pieces.detect {|p| p.color == color && p.rook? && p.side == side }
      rook && rook.has_not_moved? && empty_path_to_rook?(board, rook)
    end

    def empty_path_to_rook?(board, rook)
      direction = rook.side == :queen ? -1 : 1
      distance = (rook.position.file_index - position.file_index).abs
      intervening_piece_positions = (1..distance - 1).map do |file_offset|
        position.offset_by(file_offset * direction, 0)
      end
      intervening_piece_positions.all? {|position| board.piece_at_position(position).nil? }
    end
  end
end
