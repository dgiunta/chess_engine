module Chess
  class Rook < Piece
    MOVE_PATTERN = [
      (1..8).map {|i| [0, -i] }, # up
      (1..8).map {|i| [i, 0] }, # right
      (1..8).map {|i| [0, i] }, # down
      (1..8).map {|i| [-i, 0] }, # left
    ]

    attr_reader :side

    def initialize(*args)
      super(*args)
      @side = case position.file
      when :a; :queen
      when :h; :king
      end
    end
  end
end
