module Chess
  class Bishop < Piece
    MOVE_PATTERN = [
      (1..8).map {|i| [i, -i] }, # right and up
      (1..8).map {|i| [i, i] }, # right and down
      (1..8).map {|i| [-i, i] }, # left and down
      (1..8).map {|i| [-i, -i] } # left and up
    ]
  end
end
