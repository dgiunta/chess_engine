module Chess
  class Knight < Piece
    MOVE_PATTERN = [
      [1, -2], [2, -1], [2, 1], [1, 2], [-1, 2], [-2, -1], [-2, 1], [-1, -2]
    ].map {|move| [move] }
    def move_pattern_in(board)
      MOVE_PATTERN
    end
  end
end
