module Chess
  class Pawn < Piece
    def attackable_positions_in(board)
      [
        position.offset_by(-1, move_direction),
        position.offset_by(1, move_direction)
      ].compact.map {|pos| [pos] }
    end

    def attack_positions_in(board)
      attackable_positions_in(board).map do |path|
        path.select do |position|
          captureable_piece_at_position(board, position) ||
            en_passant_captureable_piece_for_position(board, position)
        end
      end.reject(&:empty?)
    end

    def move_positions_in(board)
      positions = [
        position.offset_by(0, move_direction),
        (position.offset_by(0, move_direction * 2) unless has_moved?)
      ].compact
      [available_positions_from(board, positions, include_existing_piece: false, pass_through_king: false)]
    end

    def available_positions_in(board)
      (move_positions_in(board) + attack_positions_in(board)).flatten
    end

    def captureable_piece_at_position(board, position)
      piece = board.piece_at_position(position)
      piece if piece && piece.captureable_by?(self)
    end

    def en_passant_captureable_piece_for_position(board, position)
      return false if position.nil?

      last_move = board.moves.last
      piece = last_move && last_move.piece

      return false unless last_move && piece && piece.pawn?

      piece.previous_positions.length == 1 &&
        piece.position == position.offset_by(0, -move_direction)
    end

    def move_direction
      black? ? 1 : -1
    end
  end
end
