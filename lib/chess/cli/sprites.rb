module Chess
  class CLI
    module Sprites
      include Colors

      def self.border(char)
        [BORDER, char, RESET].join
      end

      C_TL = border("┏")
      C_BL = border("┗")
      C_TR = border("┓")
      C_BR = border("┛")

      BH_SM = border("─")
      BV_SM = border("│")

      BH_LG = border("━")
      BV_LG = border("┃")

      TT_LG = border("┯")
      T_SM  = border("┼")
      TB_LG = border("┷")

      TL_LG = border("┠")
      TR_LG = border("┨")

      AVAILABLE_MOVE_MARKER = "#{AVAILABLE_MOVE}◼#{RESET}"
    end
  end
end
