module Chess
  class CLI
    class Board
      include Sprites

      NUM_CELLS = 7
      CELL_WIDTH = 5

      attr_reader :board, :selected_piece, :rotate
      alias_method :rotate?, :rotate

      def initialize(board:, selected_piece: nil, rotate: false)
        @board = board
        @selected_piece = selected_piece
        @rotate = false
      end

      def to_s
        [
          "",
          ((render_captured_pieces(opposing_captured_pieces) + "\n") if opposing_captured_pieces.any?),
          files_row,
          top_border,
          rows.join("\n" + horizontal_separator + "\n"),
          bottom_border,
          files_row,
          (("\n" + render_captured_pieces(my_captured_pieces)) if my_captured_pieces.any?),
        ].compact.map(&:to_s).join("\n")
      end

      def current_move
        @current_move ||= board.moves.last
      end

      def current_color
        rotate? ? current_move.color : :white
      end

      def opposing_color
        {black: :white, white: :black}[current_color]
      end

      def opposing_captured_pieces
        captured_pieces_for(opposing_color)
      end

      def my_captured_pieces
        captured_pieces_for(current_color)
      end

      def render_captured_pieces(pieces)
        if pieces.any?
          piece_color = pieces.first.color == :white ? WHITE_PIECE : BLACK_PIECE
          pieces.group_by(&:type_abbrev).map do |abbrev_name, grouped_pieces|
            count = grouped_pieces.length
            name_and_count = count > 1 ? [abbrev_name, count].join("x") : abbrev_name
            [piece_color, " #{name_and_count} ", RESET]
          end.join(" ")
        end
      end

      def captured_pieces_for(color)
        other_color = color == :white ? :black : :white
        board.captured_pieces.select {|p| p.color == other_color }
      end

      def top_border
        ["   ", C_TL, *(0..NUM_CELLS).map { (BH_LG * CELL_WIDTH) }.join(TT_LG), C_TR].join
      end

      def bottom_border
        ["   ", C_BL, *(0..NUM_CELLS).map { (BH_LG * CELL_WIDTH) }.join(TB_LG), C_BR].join
      end

      def horizontal_separator
        ["   ", TL_LG, *(0..NUM_CELLS).map { (BH_SM * CELL_WIDTH) }.join(T_SM), TR_LG].join
      end

      def files_row
        @files_row ||= "     #{files.join("     ")}"
      end

      def rows
        @rows ||= ranks.map {|rank| Row.new(self, rank) }
      end

      def files
        return @files if defined? @files

        @files = Position::FILES
        @files = @files.reverse if rotate? && current_color == :black
        @files
      end

      def ranks
        return @ranks if defined? @ranks

        @ranks = Position::RANKS
        @ranks = @ranks.reverse if rotate? && current_color == :black
        @ranks
      end

      def next_cells
        @next_cells ||= board.available_moves[selected_piece] if selected_piece
      end
    end
  end
end
