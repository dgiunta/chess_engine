module Chess
  class CLI
    class Cell
      include Sprites

      EMPTY_SPACE = "     "
      AVAILABLE_SPACE = "  #{AVAILABLE_MOVE_MARKER}  "

      attr_reader :board, :game, :position, :piece

      def initialize(board, position)
        @board = board
        @position = position
        @piece = board.board.piece_at_position(position)
      end

      def to_s
        if piece
          piece_marker
        elsif movable?
          AVAILABLE_SPACE
        else
          EMPTY_SPACE
        end
      end

      def piece_marker
        [take_marker, piece_color, " #{piece.type_abbrev} ", RESET, take_marker, RESET].join
      end

      def take_marker
        color = takeable? ? TAKE_PIECE : ""
        [color, " "].join
      end

      def movable?
        next_cells && next_cells.include?(position)
      end

      def takeable?
        movable? && piece
      end

      def next_cells
        @next_cells = board.next_cells
      end

      def piece_color
        @color ||= piece.white? ? WHITE_PIECE : BLACK_PIECE if piece
      end
    end
  end
end
