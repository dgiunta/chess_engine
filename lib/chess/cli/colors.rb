module Chess
  class CLI
    module Colors
      def self.color(fg: nil, bg: nil)
        ESC + [
          ([FG_PREFIX, fg].join(SEP) if fg),
          ([BG_PREFIX, bg].join(SEP) if bg),
        ].compact.join(SEP) + TERM
      end

      ESC = "\033["
      TERM = "m"
      SEP = ";"
      FG_PREFIX = [38, 5].join(SEP)
      BG_PREFIX = [48, 5].join(SEP)

      WHITE_PIECE = color(fg: 232, bg: 253)
      BLACK_PIECE = color(fg: 253, bg: 232)
      AVAILABLE_MOVE = color(fg: 28)
      TAKE_PIECE = color(fg: 255, bg: 124)

      BORDER = color(fg: 8)

      ERROR = color(fg: 124)
      RESET = "#{ESC}0#{TERM}"
    end
  end
end
