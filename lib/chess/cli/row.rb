module Chess
  class CLI
    class Row
      include Sprites

      attr_reader :board, :rank

      def initialize(board, rank)
        @board = board
        @rank = rank
      end

      def to_s
        [" #{rank} ", BV_LG, cells.join(BV_SM), BV_LG, " #{rank} "].map(&:to_s).join
      end

      def cells
        @cells ||= board.files.map {|file| Cell.new(board, Position.new(file: file, rank: rank)) }
      end
    end
  end
end
