module Chess
  class Move
    attr_reader :piece_class, :position, :disambiguator, :is_capture, :is_check, :is_checkmate, :is_castle, :promoted_to
    attr_accessor :color, :piece, :captured_piece

    def initialize(
      piece_class:,
      position:,
      color: nil,
      disambiguator: nil,
      promoted_to: nil,
      captured_piece: nil,
      is_capture: false,
      is_check: false,
      is_checkmate: false,
      is_castle: false
    )
      @piece_class = piece_class
      @position = position

      @color = color
      @disambiguator = disambiguator
      @promoted_to = promoted_to
      @captured_piece = captured_piece
      @is_capture = is_capture
      @is_check = is_check
      @is_checkmate = is_checkmate
      @is_castle = is_castle
    end

    alias_method :capture?, :is_capture
    alias_method :check?, :is_check
    alias_method :checkmate?, :is_checkmate
    alias_method :castle?, :is_castle

    def ==(other)
      equality_methods = %i(
        piece_class position color disambiguator
        is_capture is_check is_checkmate promoted_to
      )

      other.class == self.class && equality_methods.all? {|meth| self.send(meth) == other.send(meth) }
    end

    def summarize
      if castle?
        position.side == :king ? "0-0" : "0-0-0"
      else
        [
          piece_class.summarize,
          (disambiguator.summarize if disambiguator),
          ("x" if capture?),
          position.summarize,
          ("+" if check?),
          ("#" if checkmate?)
        ].compact.join
      end
    end

    def to_s
      [
        color.capitalize,
        piece_class.name,
        ("from #{disambiguator}" if disambiguator),
        (capture? ? "takes the piece at" : "to"),
        position,
        ("#{side.capitalize} side castle" if castle?),
        ("CHECK!" if check?),
        ("CHECKMATE!!" if checkmate?)
      ].compact.join(" ")
    end

    def side
      position.side
    end

    def white?
      color == :white
    end

    def black?
      color == :black
    end
  end
end
