module Chess
  class MoveBuilder
    attr_reader :board, :piece, :to_position, :promoted_to, :color, :piece_class

    def initialize(board:, piece:, to_position:, promoted_to: nil)
      @board = board
      @piece = piece

      @color = piece.color
      @piece_class = piece.class
      @to_position = to_position
      @promoted_to = promoted_to
    end

    def build
      Move.new(
        piece_class: piece_class,
        position: to_position,
        color: color,
        disambiguator: disambiguator,
        promoted_to: promoted_to,
        captured_piece: captured_piece,
        is_capture: !!captured_piece,
        is_castle: is_castle,
        is_check: false,
        is_checkmate: false
      )
    end

    def disambiguator
      return nil if possible_pieces.length == 1

      from_position = piece.position
      positions = possible_pieces.map(&:position)

      need_rank = positions.select {|p| p.file == from_position.file }.length > 1
      need_file = positions.select {|p| p.rank == from_position.rank }.length > 1

      file = piece.position.file if need_file
      rank = piece.position.rank if need_rank

      Position[file, rank]
    end

    def possible_pieces
      @posible_pieces ||= board.available_moves.pieces_by_position(to_position).select do |p|
        p.color == color && p.is_a?(piece_class)
      end
    end

    def is_castle
      piece.is_a?(King) && piece.castleable?(board, to_position.side)
    end

    def captured_piece
      @captured_piece ||= board.piece_at_position(to_position)
    end
  end
end
