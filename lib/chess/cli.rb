require 'chess/cli/colors'
require 'chess/cli/sprites'
require 'chess/cli/cell'
require 'chess/cli/row'
require 'chess/cli/board'

module Chess
  class CLI2
    attr_reader :game

    def self.start
      new.start
    end

    def initialize
      @game = Game.new
    end

    def start
      loop do
        puts current_board

        next_piece = selected_piece
        next if next_piece.nil?

        next unless next_positions_for(next_piece).any?

        puts current_board.to_s(next_piece)
        next_position = selected_next_position_for(next_piece)

        promoted_to = selected_promotion_class if promotable?(next_piece, next_position)

        game.add_move(piece: next_piece, to: next_position, promoted_to: promoted_to)
      end
    end

    def selected_piece
      puts "Enter the position of the piece you want to move? (#{game.current_color})"

      current_pieces = pieces_with_available_moves
      current_pieces = current_pieces.reverse if game.current_color == :black

      current_pieces.each do |piece|
        puts piece
      end

      file, rank = get_position_input

      selected_position = Position[file, rank]
      current_board.piece_at_position(selected_position)
    end

    def selected_next_position_for(piece)
      next_positions = next_positions_for(piece)
      puts "Available moves for #{piece.class.name}:"
      next_positions.each do |next_pos, i|
        puts next_pos
      end

      puts "Where do you want to move #{piece.class.name}?"
      file, rank = get_position_input
      Position[file, rank]
    end

    def current_board
      game.current_board
    end

    def pieces_with_available_moves
      current_board.pieces.select do |p|
        p.color == game.current_color && current_board.available_moves.positions_by_piece(p).any?
      end
    end

    def promotable?(piece, position)
      return false unless piece.pawn?
      rank = piece.white? ? 8 : 1
      position.rank == rank
    end

    def get_position_input
      input = gets.chomp
      file, rank = input.chars
      [file.to_sym, rank.to_i]
    end

    def next_positions_for(piece)
      current_board.available_moves.positions_by_piece(piece)
    end
  end

  class CLI
    class Exit < StandardError; end
    class Next < StandardError; end
    class NoPieceAtPosition < StandardError
      def initialize(position)
        @position = position
      end

      def to_s
        "There's no piece at position #{@position}"
      end
    end

    include Sprites
    attr_reader :game

    def self.start(options)
      new.start
    end

    def initialize(game=Game.new)
      @game = game
    end

    def start
      loop do
        begin
          move(selected_piece)
        rescue Exit
          puts "Bye!"
          exit
        rescue Next
          next
        rescue NoPieceAtPosition => e
          puts e
          next
        end
      end
    end

    def current_color
      game.current_color
    end

    def selected_piece
      puts Board.new(game.current_board)
      puts "Enter the position of the piece you want to move? (#{current_color})"

      current_pieces = game.pieces_for_color(current_color).select {|p| game.next_available_cells_for(p).flatten.any? }
      current_pieces = current_pieces.reverse if current_color == :black

      current_pieces.map do |piece|
        puts [piece.position, piece.type].join(" - ")
      end

      position_input = get_position_input
      raise Next unless position_input

      selected_position = Position.parse(position_input)
      game.piece_at_position(selected_position).tap do |piece|
        raise Next if piece.color != current_color
        raise NoPieceAtPosition.new(selected_position) if piece.nil?
      end
    end

    def move(piece)
      selected_piece_class = piece.class.to_s.split("::").last
      next_positions = game.next_available_cells_for(piece).flatten

      if next_positions.any?
        puts Board.new(game, piece)

        puts "Available moves for #{selected_piece_class}:"
        next_positions.each do |next_pos, i|
          puts next_pos
        end
        puts "x  - select a different piece"

        puts "Where do you want to move #{selected_piece_class}?"
        selected_position = Position.parse(get_position_input)
        begin
          game.add_move(piece, selected_position)
          game.current_color = game.opposing_color
        rescue Game::InvalidMove => e
          puts error(e)
          move(piece)
        end
      else
        puts error("No moves for #{selected_piece_class}. Try again.")
      end
    end

    def get_position_input
      get_input.tap do |str|
        raise Next if str.length < 2
      end
    end

    def get_input
      print ">> "
      input = gets.chomp
      case input
      when /^(exit|quit|q)$/
        raise Exit
      when /^(cancel|x)$/
        raise Next
      else
        input
      end
    end

    def color(fg: nil, bg: nil)
      color_prefix = "\033["
      fg_prefix = "38;5;"
      bg_prefix = "48;5;"

      [
        color_prefix,
        ([fg_prefix, fg].join if fg),
        ([bg_prefix, bg].join if bg),
      ].compact.join(";") + "m"
    end

    def error(text)
      [ERROR, text, RESET]
    end
  end
end
