module Chess
  class AvailablePositionsCalculator
    attr_reader :board

    def initialize(board)
      @board = board
    end

    def king_positions
      @king_positions ||= position_config[:king_positions]
    end

    def position_config
      @position_config ||= board.pieces.reduce({king_positions: []}) do |out, piece|
        positions = piece.available_positions_in(board)

        out[piece.color] ||= {
          pawns: {},
          by_piece: {},
          occupied_positions: [],
          move_positions: []
        }

        if piece.king?
          out[piece.color][:king] = piece
          out[:king_positions] << piece.position
        end

        if piece.pawn?
          out[piece.color][:pawns][piece] = positions
        end

        out[piece.color][:by_piece][piece] = positions

        out[piece.color][:occupied_positions] << piece.position

        positions.each do |position|
          out[piece.color][:move_positions] << position
        end

        out
      end
    end
  end
end
