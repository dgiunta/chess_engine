module Chess
  class BoardCache
    def initialize(game)
      @game = game
      @boards = []
    end

    def board_for_move(move)
      index = @game.moves.index(move)
      board_for_move_at_index(index)
    end

    def board_for_move_at_index(index)
      @boards[index] ||= Board.new(moves: @game.moves[0..index])
    end
  end

  class Game
    COLORS = {
      'W' => :white,
      'B' => :black
    }

    STARTING_PIECES = %W[
      BRa8 BNb8 BBc8 BQd8 BKe8 BBf8 BNg8 BRh8
      BPa7 BPb7 BPc7 BPd7 BPe7 BPf7 BPg7 BPh7
      WPa2 WPb2 WPc2 WPd2 WPe2 WPf2 WPg2 WPh2
      WRa1 WNb1 WBc1 WQd1 WKe1 WBf1 WNg1 WRh1
    ].map do |str|
      color, piece, file, rank = str.chars
      [COLORS[color], Piece.class_from(piece), file.to_sym, rank.to_i]
    end.freeze

    attr_reader :rounds, :moves, :board_cache

    def initialize(rounds: [Round.new(number: 1)])
      @rounds = rounds
      @moves = rounds.flat_map(&:moves)
      @board_cache = BoardCache.new(self)
    end

    def summarize
      rounds.map(&:summarize).join("\n")
    end

    def add_move(piece:, to:, promoted_to: nil)
      move = MoveBuilder.new(
        board: current_board,
        piece: piece,
        to_position: to,
        promoted_to: promoted_to
      ).build

      round = current_round.complete? ? add_round : current_round
      round.moves << move
      moves << move
      move
    end

    def add_round
      Round.new(number: current_round.number + 1).tap do |round|
        rounds << round
      end
    end

    def current_round
      rounds.last
    end

    def current_board
      index = moves.length > 0 ? moves.length - 1 : 0
      board_cache.board_for_move_at_index(index)
    end

    def board_for_move_at_index(i)
      board_cache.board_for_move_at_index(i)
    end

    def current_move
      moves.last
    end

    def current_color
      current_round.current_color
    end

    def opposing_color
      opposite_color_of(color)
    end

    private

    def opposite_color_of(color)
      {black: :white, white: :black}[current_color]
    end
  end
end
