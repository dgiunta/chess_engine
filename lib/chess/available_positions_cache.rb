module Chess
  class AvailablePositionsCache
    include Enumerable

    attr_reader :board, :available_moves

    def initialize(board)
      @board = board
      @available_moves = compile_available_moves
    end

    def each(&block)
      available_moves.each(&block)
    end

    def [](key)
      available_moves[key]
    end

    def find_moves_by(key)
      available_moves.select {|k, _pieces| k.class == key.class && k == key }.flat_map {|_k, v| v } || []
    end

    def positions_by_piece(piece)
      find_moves_by(piece)
    end

    def pieces_by_position(position)
      find_moves_by(position)
    end

    def raw_positions
      @raw_positions ||= board.pieces.reduce({king_positions: []}) do |out, piece|
        attackable_positions = piece.attackable_positions_in(board)
        move_positions = piece.available_positions_in(board)

        out[piece.color] ||= {
          by_piece: {},
          move_positions: [],
          occupied_positions: [],
          attackable_positions: []
        }

        if piece.king?
          out[piece.color][:king] = piece
          out[:king_positions] << piece.position
        end

        out[piece.color][:by_piece][piece] ||= {}
        out[piece.color][:by_piece][piece][:attackable_positions] = attackable_positions
        out[piece.color][:by_piece][piece][:move_positions] = move_positions

        out[piece.color][:occupied_positions] << piece.position

        move_positions.each do |position|
          out[piece.color][:move_positions] << position
        end

        attackable_positions.flatten.each do |position|
          out[piece.color][:attackable_positions] << position
        end

        out
      end.tap do |config|
        [:white, :black].each do |color|
          config[color] ||= {
            by_piece: {},
            occupied_positions: [],
            move_positions: [],
            attackable_positions: []
          }
        end
      end
    end

    def compile_available_moves
      kings_positions = raw_positions[:king_positions]

      {white: :black, black: :white}.reduce({}) do |out, (color, opposing_color)|
        config = raw_positions[color]
        king = config[:king]
        pieces = config[:by_piece]
        piece_positions = config[:occupied_positions]

        opposing_config = raw_positions[opposing_color]
        opposing_pieces = opposing_config[:by_piece]
        opposing_attackable_positions = opposing_config[:attackable_positions]

        attacking_piece = nil
        attacking_piece_positions = []
        opposing_pieces.detect do |piece, opp_config|
          positions = opp_config[:attackable_positions]
          attack_path = positions.detect do |path|
            path.include?(king.position) if king
          end

          if attack_path
            attacking_piece = piece
            attacking_piece_positions = attack_path
          end
        end

        in_check = !attacking_piece.nil?

        if in_check
          # reduce available positions to those that get the king out of check
          pieces = pieces.inject({}) do |out, (piece, config)|
            if piece.king?
              config[:move_positions] = config[:move_positions].reject {|position| opposing_attackable_positions.include?(position) }
            else
              config[:move_positions] = config[:move_positions].select {|position| attacking_piece_positions.include?(position) || position == attacking_piece.position }
            end
            out[piece] = config
            out
          end
        end

        # remove any move positions that have another one of our own pieces on them
        pieces = pieces.reduce({}) do |out, (piece, config)|
          positions = config[:move_positions].flatten
          next_positions = positions.reject do |position|
            piece_positions.include?(position) || kings_positions.include?(position)
          end
          if piece.king?
            next_positions = next_positions.reject {|position| opposing_attackable_positions.include?(position) }
          end
          out[piece] = next_positions
          out
        end

        out.merge(pieces)
      end.reduce({}) do |out, (piece, positions)|
        out[piece] = positions
        positions.each do |position|
          out[position] ||= []
          out[position] << piece
        end
        out
      end
    end
  end
end
