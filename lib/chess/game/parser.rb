module Chess
  class Game
    class Parser < Parslet::Parser
      rule(:nothing)       { any.absent? }
      rule(:eol)           { match["\n"].repeat(1).repeat(1) }
      rule(:eol?)          { eol.maybe }

      rule(:space)         { match('\s').repeat(1) }
      rule(:space?)        { space.maybe }

      rule(:round_num) do
        (match["0-9"].repeat(1).as(:integer)).as(:round) >>
        str(".") >>
        space?
      end

      rule(:rank)          { match["1-8"].as(:integer).maybe.as(:rank) }
      rule(:file)          { match["a-h"].as(:symbol).maybe.as(:file) }

      rule(:piece_ident)   { match["QKBNRP"] }
      rule(:piece?)        { piece_ident.maybe.as(:piece_ident).as(:piece).maybe }

      rule(:position)      { (file >> rank) }
      rule(:position?)     { (file.maybe >> rank.maybe) }

      rule(:is_capture?)   { match["x:"].maybe.as(:bool).maybe.as(:is_capture) }
      rule(:is_check?)     { space? >> str("+").maybe.as(:bool).maybe.as(:is_check) }
      rule(:is_checkmate?) { str("#").maybe.as(:bool).maybe.as(:is_checkmate) }
      rule(:promoted_to?)   { str("=").maybe >> piece_ident.as(:piece_ident).maybe.as(:promoted_to) }

      rule(:castle) do
        (str("0-0") | str("0-0-0")).as(:castle)
      end

      rule(:normal_move) do
        piece? >>
        position?.as(:position_1) >>
        is_capture? >>
        position.as(:position_2) >>
        promoted_to? >>
        is_check? >>
        is_checkmate?
      end

      rule(:move) { normal_move | castle }
      rule(:move?)         { move.maybe }

      rule(:moves)         { move.as(:white) >> space? >> move?.as(:black) }
      rule(:round)         { round_num >> moves.as(:moves) >> (space? | eol?) }

      rule(:game)          { round.repeat(1).as(:rounds) | eol }

      root :game
    end
  end
end
