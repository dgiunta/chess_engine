module Chess
  class Game
    class AST < Parslet::Transform
      rule(integer: simple(:int)) { int.to_i }
      rule(symbol: simple(:sym)) { sym.to_sym }
      rule(bool: simple(:bool)) { !!(bool && bool.length > 0) }

      # pieces
      rule(piece_ident: simple(:piece_ident)) { Piece.class_from(piece_ident) }

      # positions
      rule(file: simple(:file), rank: simple(:rank)) do
        Position.new(file: file, rank: rank) unless file.nil? && rank.nil?
      end

      # castle
      rule(castle: simple(:castle)) do
        file = case castle
        when "0-0"; :g
        when "0-0-0"; :c
        end
        position = Position.new(file: file, rank: nil)
        Move.new(piece_class: King, position: position, is_castle: true)
      end

      # moves
      rule(
        piece: simple(:piece),
        position_1: simple(:position_1),
        is_capture: simple(:is_capture),
        position_2: simple(:position_2),
        promoted_to: simple(:promoted_piece),
        is_check: simple(:is_check),
        is_checkmate: simple(:is_checkmate)
      ) do
        position = position_2 ? position_2 : position_1
        disambiguator = position_2 ? position_1 : nil

        Move.new(
          piece_class: piece,
          position: position,
          disambiguator: disambiguator,
          is_capture: is_capture,
          promoted_to: promoted_piece,
          is_check: is_check,
          is_checkmate: is_checkmate
        )
      end

      # add color to moves based on position
      rule(white: simple(:white_move), black: simple(:black_move)) do
        white_move.color = :white
        black_move.color = :black if black_move

        [white_move, black_move].compact.each do |move|
          move.position.rank = move.white? ? 1 : 8 if move.castle?
        end
      end

      rule(round: simple(:number), moves: sequence(:moves)) { Round.new(number: number, moves: moves) }
      rule(rounds: sequence(:rounds)) do
        Game.new(rounds: rounds)
      end
    end
  end
end
