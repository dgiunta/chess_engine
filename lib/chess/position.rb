module Chess
  class Position
    attr_accessor :file, :rank

    RANKS = (1..8).to_a.reverse
    FILES = (:a..:h).to_a

    def self.[](file, rank)
      new(file: file, rank: rank)
    end

    def self.offset_from(start_position, file_offset, rank_offset)
      file_index = start_position.file_index + file_offset
      rank_index = start_position.rank_index + rank_offset

      out_of_bounds = file_index.negative? || file_index >= FILES.length ||
        rank_index.negative? || rank_index >= RANKS.length

      return nil if out_of_bounds

      file = FILES[file_index]
      rank = RANKS[rank_index]

      new(file: file, rank: rank)
    end

    def initialize(file:, rank:)
      @file, @rank = file, rank
    end

    def summarize
      [file, rank].join
    end

    def to_s
      [file, rank].join("-")
    end

    def offset_by(file_offset, rank_offset)
      self.class.offset_from(self, file_offset, rank_offset)
    end

    def ==(other)
      other && file == other.file && rank == other.rank
    end

    def similar_to?(other)
      file_matches = file == other.file if other.file
      rank_matches = rank == other.rank if other.rank

      matches = [file_matches, rank_matches].compact
      matches.any? && matches.all? {|m| m == true }
    end

    def empty?
      file.nil? && rank.nil?
    end

    def present?
      !empty?
    end

    def side
      file_index <= 3 ? :queen : :king
    end

    def file_index
      FILES.index(file)
    end

    def rank_index
      RANKS.index(rank)
    end
  end
end
